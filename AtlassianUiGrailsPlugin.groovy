class AtlassianUiGrailsPlugin {

    def version = "0.1.0"
    def grailsVersion = "2.0 > *"

    def title = "Atlassian Ui Plugin"
    def author = "Ilya Telegin"
    def authorEmail = "ilya.a.telegin@gmail.com"
    def description = 'Styles and scripts for Atlassian UI (https://docs.atlassian.com/aui/latest/)'

    def documentation = "http://grails.org/plugin/atlassian-ui"


    def license = "GPL3"

    def developers = [ [ name: "Ilya Polotsky", email: "ipolo.box@gmail.com" ]]

    def issueManagement = [ system: "Bitbucket", url: "https://bitbucket.org/treeice/grails-atlassian-ui/issues" ]

    def scm = [ url: "https://bitbucket.org/treeice/grails-atlassian-ui" ]

}
